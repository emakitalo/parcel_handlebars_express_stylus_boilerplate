const express = require("express");
const app = express();
const hbs = require("express-handlebars");
const fs = require("fs");
const fsAsync = require("fs").promises;
const readFile = fsAsync.readFile;
const lstat = fsAsync.lstat;
const markdown = require("markdown-it");
const md = new markdown({ html: true });

const siteName = "esa mäkitalo";
const viewPath = "./views/";
const pages = "pages";
const pagesPath = viewPath + pages;
const dataPath = viewPath + "data/";
const navigation = JSON.parse(fs.readFileSync(dataPath + "navigation.json"));
const footer = JSON.parse(fs.readFileSync(dataPath + "footer.json"));

//process.env.NODE_ENV === "production";

app.use(express.static("dist"));
app.engine(
    ".hbs",
    hbs({
        extname: ".hbs",
        helpers: {
            log: (data) => {
                console.log(data);
            },
            subPath: (data) => {
                return data.length > 1 ? true : false;
            },
            concat: (str1, str2) => {
                return str1 + "/" + str2;
            },
            md: (data) => {
                return md.render(data);
            }
        }
    })
);
app.set("view engine", ".hbs");

function getFile(name) {
    return readFile(name);
}

function getPath(nav, path, breadcrumb) {
    for (const [k, v] of Object.entries(nav)) {
        const p = v.path.split("/").reverse()[0];
        if (p === path[0]) {
            const d = path.shift();
            breadcrumb[k] = {
                path: v.path
            };
            if (v.children) {
                breadcrumb[k]["children"] = {};
                getPath(v.children, path, breadcrumb[k].children);
            }
        }
    }
    return breadcrumb;
}

function getContent(nav, path) {
    for (const [k, v] of Object.entries(nav)) {
        const p = v.path.split("/").reverse()[0];
        if (p === path[0]) {
            const d = path.shift();
            if (v.children) {
                const content = v.children;
                return getContent(content, path);
            }
        }
    }
    return nav;
}

function getPageName(nav, path) {
    let pageName = "";
    for (const [k, v] of Object.entries(nav)) {
        const p = v.path.split("/").reverse()[0];
        if (p === path[0]) {
            pageName = k;
            const d = path.shift();
            if (v.children && path.length > 0) {
                const content = v.children;
                return getPageName(content, path);
            }
        }
    }
    return pageName;
}

app.get("/*", (req, res) => {
    const file = req.path.split(".").length > 1 ? "/error" : req.path;
    const pathBC = file.split("/");
    pathBC.shift();
    const pathC = [...pathBC];
    const nav = navigation.children;
    const pageName = getPageName(nav, [...pathBC]);
    const breadcrumb = {
        children: {
            "home": {
                path: "",
                children: getPath(nav, pathBC, {})
            }
        }
    };
    lstat(pagesPath + file)
        .then((data) => {
            if (data.isDirectory()) {
                const content = { children: getContent(nav, pathC, {}) };
                res.render(pages + file + "/index", {
                    siteName: siteName,
                    path: file,
                    navigation: navigation,
                    footer: footer,
                    breadcrumb: breadcrumb,
                    list: content,
                    pageName: pageName
                });
            }
        })
        .catch((err2) => {
            getFile(pagesPath + file + ".hbs")
                .then((page) => {
                    res.render(pages + file, {
                        siteName: siteName,
                        path: file,
                        navigation: navigation,
                        footer: footer,
                        breadcrumb: breadcrumb,
                        pageName: pageName
                    });
                })
                .catch((err1) => {
                    res.render(pages + "/error", {
                        siteName: siteName,
                        page: file,
                        navigation: navigation,
                        footer: footer,
                        breadcrumb: breadcrumb,
                        pageName: pageName
                    });
                });
        });
});

app.listen(9001);
